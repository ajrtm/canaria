#!/bin/bash

cd ~/Desktop/Development/Source.old/Canaria
git fetch -v
git add --all --verbose
git commit --allow-empty-message -m ''
git push -v
nodes=( robert theo )

for ((i = 0; i < ${#nodes[@]}; i++)) do

    printf "\n\n\n"
    echo ${nodes[$i]}
    printf "===============================================\n"

    ssh ${nodes[$i]} '
        apt-get autoremove
        apt-get -y upgrade

        cd /var/www/canaria

        chmod -R 777 /var/www/canaria/0/app/storage

        git fetch
        git checkout master --force
        git pull -v

        cd /etc/apache2/sites-available

        cp -f /var/www/canaria/canaria.conf .
        rm ../sites-enabled/canaria.conf

        sudo ln /etc/apache2/sites-available/canaria.conf /etc/apache2/sites-enabled/canaria.conf

        sudo service apache2 restart

        exit
    '

done

printf "\n\n\n\n - - - - - - - - - - - - - - - - - - - - - - - - - - - - \n\n\n\n"

git status
