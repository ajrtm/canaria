<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array(
    'as' => 'home',
    'uses' => 'HomeController@showWelcome'
));

Route::get('/about', function() {
    return View::make('v.about.en-US.index');
});

Route::get('/gallery', function() {
    return View::make('v.gallery.index');
});

Route::get('/contact', function() {
    return View::make('v.contact.en-US.index');
});

Route::get('/faq', function() {
    return View::make('v.faq.en-US.index');
});

Route::get('/prices', function() {
    return View::make('v.pricing.en-US.index');
});

// CSRF protection group
Route::group(array('before' => 'csrf'), function()
{
    // Contact form (POST)
    Route::post('/contact/process', array(
        'as'    => 'contact-form-post',
        'uses'  => 'ContactController@postContactForm'
    ));
});