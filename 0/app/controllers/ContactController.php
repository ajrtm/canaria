<?php

class ContactController extends BaseController
{
    public function postContactForm()
    {

        $validator = Validator::make(Input::all(), array(
            'name'          => 'required',
            'telephone'     => 'required',
            'email'         => 'required|email'
        ));

        if($validator->fails())
        {
            // Redirect back to the account creation page
            return Redirect::route('home')
                -> withErrors($validator)
                -> withInput()
                -> with('global', 'You did not fill out the contact form correctly, please review the error messages and try again');
        }
        else
        {

            $name       = Input::get('name');
            $telephone  = Input::get('telephone');
            $email      = Input::get('email');
            $question   = Input::get('question');
            $detail     = Input::get('detail');
            $ip         = Input::get('ip');

            $data = array(
                'name'          => $name,
                'telephone'     => $telephone,
                'email'         => $email,
                'question'      => $question,
                'detail'        => $detail,
                'ip'            => $ip,
            );

            // Send email
            Mail::send('global.external.mail.form', array('name' => $name, 'telephone' => $telephone, 'email' => $email, 'question' => $question, 'detail' => $detail, 'ip' => $ip), function($message) use ($data)
            {
                $message->to("info@carpacanaria.com", "Carpa-Canaria Site")->cc("ss_canaria@developka.com")->subject('Contact Form Submission');
                $message->from("postmaster@developka.com", "Carpa-Canaria Form Agent");
            });

            // Return back to the login page with a success message
            return Redirect::route('home')
                -> with('global', 'The form was submitted and someone will contact you shortly');
        }
    }
}