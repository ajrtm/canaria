<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta http-equiv="refresh" content="3000" />
<meta name="google-translate-customization" content="1e3c35f2d2274ced-7fab5bcd77cafbfd-g3b94bab1d83996b4-15"></meta>
<link rel="home" href="http://www.carpacanaria.com/" />
<link rel="stylesheet" type="text/css" href="/resources/external/bootstrap/styles/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/resources/global/styles/base.css" />
<link rel="stylesheet" type="text/css" href="/resources/global/styles/navigation.css" />
<link rel="stylesheet" type="text/css" href="/resources/global/styles/enhanced.css" />
<link rel="stylesheet" type="text/css" href="/resources/global/styles/fonts.css" />
<style>
	html, body, #loader {
		-webkit-transition: background-color 1s, display 5s;
		-o-transition: background-color 1s, display 5s;
		transition: background-color 1s, display 5s;
	}
	html, body {
		background-color: white;
	}
	#main {
		display: none;
		z-index: 0;
	}
	#loader {
		padding: 240px 0;
		position: fixed;
		width: 100%;
		height: 100%;
		top: 0;
		left: 0;
		z-index: 9999;
		background-color: black !important;
	}
	#loader h1 {
		color: white;
		font-size: 72px;
		text-align: center;
	}
</style>
<script type="text/javascript" src="/resources/external/jquery/jquery.min.js"></script>
<script type="text/javascript">

	$(document).ready(function()
	{
		$("#main").delay(2000).css("background", "white").fadeIn(1200, function() {
			$("#loader").css("background-color", "none");
			$("#loader h1").html("We're ready!");
			$("#loader").delay(500).fadeOut(1200);
		});

	});
</script>