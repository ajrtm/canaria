    @include('global.form')
    <footer class="cc-gf">
        <div class="_l">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <ul>
                            <li><a href="/">home</a></li>
                            <li><a href="/about">about</a></li>
                            <li><a href="/faq">FAQ</a></li>
                            <li><a href="/prices">Prices</a></li>
                            <li><a href="/contact">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul>
                            <li><a href="/gallery">Gallery</a></li>
                            <li><a href="http://facebook.com/carpacanaria">Facebook</a></li>
                            <!-- <li><a href="#">Twitter</a></li> -->
                            <li><a href="http://instagram.com/carpa_canaria">Instagram</a></li>
                            <li><a href="http://status.carpacanaria.com" target="_blank">Service Status</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-md-offset-1">
                        <ul>
                            <li>get updates</li>
                            <li>
                                <form action="//developka.us9.list-manage.com/subscribe/post?u=9fba232fa55c6b0ffdefbedca&amp;id=90807e1dce" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                    <div class="form-group">
                                        <input type="text" value="" name="FNAME" class="form-control" id="mce-FNAME" placeholder="First Name">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" value="" name="LNAME" id="contact-email" placeholder="Last Name" class="form-control" id="mce-LNAME">
                                    </div>
                                    <div class="form-group">
                                        <input type="email" value="" name="EMAIL" class="form-control" id="mce-EMAIL" placeholder="Email">
                                    </div>
                                  <button type="submit" class="btn btn-default">Subscribe</button>
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="_d">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <ul class="list-inline">
                            <li><a href="/contact">contact us</a></li>
                            <li><a href="mailto:info@carpacanaria.com">info@carpacanaria.com</a></li>
                            <li class="hidden">CRPX: No validation information provided.</li>
                        </ul>
                    </div>
                    <div class="col-md-4 text-right">©&nbsp;&nbsp;carpa-canaria fishing all rights reserved</div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Script -->
    <script type="text/javascript" src="/resources/external/bootstrap/scripts/bootstrap.min.js"></script>
<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(["setDocumentTitle", document.domain + "/" + document.title]);
  _paq.push(["setCookieDomain", "*.carpacanaria.com"]);
  _paq.push(["setDomains", ["*.carpacanaria.com"]]);
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//insight.ajrtm.com/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', 4]);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<noscript><p><img src="//insight.ajrtm.com/piwik.php?idsite=4" style="border:0;" alt="" /></p></noscript>
<!-- End Piwik Code -->

</div>