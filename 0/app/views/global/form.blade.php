<div class="container">
    <h1 class="text-center">Contact us today on <a href="tel:0034665038665" style="font-size: 48px;">&nbsp;&nbsp;+34 665 038 665</a></h1>
    <h1 style="margin-top: 50px; text-align: center;">Or send Carpa-Canaria Fishing an email</h1>
    <div class="row">
        <form action="{{ URL::route('contact-form-post') }}" method="post">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="name">
                        Name
                        @if($errors->has('name'))
                            <p class="text-danger">{{ $errors->first('name') }}</p>
                        @endif
                    </label>
                    <input type="text" class="form-control input-lg" name="name" placeholder="John Appleseed" {{ (Input::old('name')) ? 'value="' . Input::old('name') . '"' : '' }}>
                </div>
                <div class="form-group">
                    <label for="telephone">
                        Telephone Number
                        @if($errors->has('telephone'))
                            <p class="text-danger">{{ $errors->first('telephone') }}</p>
                        @endif
                    </label>
                    <input type="text" class="form-control input-lg" name="telephone" placeholder="01255 719819" {{ (Input::old('telephone')) ? 'value="' . Input::old('telephone') . '"' : '' }}>
                </div>
                <div class="form-group">
                    <label for="email">
                        Email Address
                        @if($errors->has('email'))
                            <p class="text-danger">{{ $errors->first('email') }}</p>
                        @endif
                    </label>
                    <input type="email" class="form-control input-lg" name="email" placeholder="john@appleseed.com" {{ (Input::old('email')) ? 'value="' . Input::old('email') . '"' : '' }}>
                </div>
                @if(!isset($defined))
                <div class="form-group">
                    <label for="question">
                        What is your question
                        @if($errors->has('question'))
                            <p class="text-danger">{{ $errors->first('question') }}</p>
                        @endif
                    </label>
                    <input type="text" class="form-control input-lg" name="question" placeholder="Do I need my passport" {{ (Input::old('question')) ? 'value="' . Input::old('question') . '"' : '' }}>
                </div>
                <div class="form-group">
                    <label for="detail">
                        More Information
                        @if($errors->has('detail'))
                            <p class="text-danger">{{ $errors->first('detail') }}</p>
                        @endif
                    </label>
                    <textarea rows="5" class="form-control input-lg" name="detail" style="word-wrap: break-word;" placeholder="Tell us more about your question. The more information you can give us, the better we can help you!" {{ (Input::old('detail')) ? 'value="' . Input::old('detail') . '"' : '' }}></textarea>
                </div>
                @endif
            </div>
            <div class="col-md-12">
                <button type="submit" style="margin: 25px 0;" class="btn btn-success btn-lg btn-block">Send</button>
            </div>
            <input type="hidden" name="ip" value="{{ $_SERVER['REMOTE_ADDR'] }}">
            {{ Form::token() }}
        </form>
    </div>
</div>