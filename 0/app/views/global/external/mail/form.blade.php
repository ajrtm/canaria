<font face="lucida grande" color="#141414" size="3">
The following message has been generated because a visitor sent you a message through your website.<br><br>

- - - - - -<br><br>
<b>Name: </b>{{ $name }} <br>
<b>Tel: </b>{{ $telephone }} <br>
<b>Email: </b>{{ $email }} <br>
<b>Question: </b>{{ $question }}<br>
<b>Information: </b>{{ $detail }}<br>
- - - - - -<br><br><br><br>
This message was sent from: <br>
<b>IP Address: </b> {{ $ip }}<br>
<br>
<i>Which can be checked against <a href="http://insight.developka.com/">http://insight.developka.com/</a> for previous usage records. <br>
Finally, you can visit <a href="http://status.carpacanaria.com/">http://status.carpacanaria.com/</a> to check if there are any known service issues. <br>
<br>
<br>