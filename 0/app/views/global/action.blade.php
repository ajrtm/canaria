<font face="Helvetica">
	<b><font color="#999" size="2">Sent by Carpa-Canaria Fishing server agent</font><br>
</font>
<font face="Helvetica">
	<b><font color="#E3310D" size="5">PRIORITY ACTION REQUIRED</font><br><br><hr><br></b>
</font>
<font face="Helvetica">
	<font color="#000" size="2">
		You are being sent this email because a fatal and unrecoverable error occured on your website. A customer visiting your website tried to request/perform an action and the server was unable to respond appropriately.<br>
		You should expect that all your services have been impacted by this problem.<br><br><br>
		The following information may be useful at this time:
		<ul>
			<li>Website Usage: <a target="_blank" href="http://insight.developka.com/">http://insight.developka.com/</a></li>
		</ul>
		<br><br>
		<b>If you are unable to visit a database server page, or the website usage page, there is a server wide problem which we have already been alerted to and we are taking action to redirect traffic to ensure the uptime and availability of your website.<br>
		Additionally, you can check that your local route-server is processing requests:</b><br>
		<a target="_blank" href="http://ross.developka.com/">http://ross.developka.com/</a><br>
		<font color="#999" size="1">You should see a page welcoming you to Nginx</font><br><br>
	</font>
	<br><hr><br>
	<font color="#000" size="2">
		If you are already aware of this problem, please ignore this email. This email is automatically sent each time the error event is triggered. Your server administration team has been notified.
		<br> <br> Developka Enterprise Support <br>hello@developka.com, michael@developka.com, ross@developka.com
	</font>
</font>