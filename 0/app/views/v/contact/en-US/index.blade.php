<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US" prefix="og: http://ogp.me/ns#">
    <head>
        <title>Carpa Canaria</title>
        @include('global.meta.header')
        <link rel="stylesheet" href="/resources/v/contact/styles/contact.css">
    </head>
    <body>
        @include('global.include.en-US.navigation')
        <div class="container">
            <div class="row title">
                <div class="col-md-12">
                    <h1>Contact Carpa Canaria</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <form role="form" method="post">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name" placeholder="John Appleseed">
                        </div>
                        <div class="form-group">
                            <label for="email">Email Address</label>
                            <input type="email" class="form-control" name="email" placeholder="example@example.com">
                        </div>
                        <div class="form-group">
                            <label for="subject">Question</label>
                            <input type="text" class="form-control" name="subject" placeholder="I have a question about my booking!">
                        </div>
                        <div class="form-group">
                            <label for="detail">Details</label>
                            <textarea class="form-control" name="detail" placeholder="Type your message here. The more information you provide, the better our agent will be able to help you." rows="5"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="tel">Telephone</label>
                            <input type="text" class="form-control" name="tel" placeholder="1-800-781-9187">
                            <p class="help-block">This is optional</p>
                        </div>
                      <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                </div>
            </div>
        </div>
        @include('global.meta.footer')
    </body>
</html>