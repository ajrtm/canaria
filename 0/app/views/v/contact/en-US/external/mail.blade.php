<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
</head>
<body>
    <b>Carpa Canaria</b><br>
    - - - - - - - - - - - - - - - - - - - - <br><br>
    Hi,<br>
    <br>
    A website visitor used the contact form located at <a href="http://www.carpacanaria.com/contact">carpacanaria.com/contact</a>.<br>
    The message is below<br><br>
    - - - - - - - - - - - - - - - - - - - -  <br><br>
    {{ nl2br($note) }}<br><br>
    - - - - - - - - - - - - - - - - - - - -  <br><br>
    <br>
    <br>
    Name: {{ $forename }} {{ $surname }}<br>
    Email address: {{ $email }}<br>
    Tel: {{ $number }}<br>
    You can respond to the message directly by responding to this message<br>
    <br>
    For support, please contact Michael Brewster by Email: hello@developka.com<br>
</body>
</html>