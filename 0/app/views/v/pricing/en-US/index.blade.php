<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US" prefix="og: http://ogp.me/ns#">
    <head>
        <title>Carpa Canaria</title>
        @include('global.meta.header')
        <link rel="stylesheet" href="/resources/v/pricing/styles/pricing.css">
    </head>
    <body>
        @include('global.include.en-US.navigation')
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Pricing</h1>
                    <p>Our current pricing is <b>40 Euros for half day fishing (20 Euros for children)</b>. Including transport, fishing equipment and all  amenities</p>
                    <p><b><a href="/contact">BOOK NOW TO SECURE YOUR PLACE!!!!!</a></b></p>
                    <p>Tel: <a href="tel:0034665038665">+(34) 665 038 665</a> <b>OR EMAIL</b> <a href="mailto:info@carpacanaria.com">info@carpacanaria.com</a></p>
                </div>
            </div>
        </div>
        @include('global.meta.footer')
    </body>
</html>