<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US" prefix="og: http://ogp.me/ns#">
    <head>
        <title>Carpa Canaria</title>
        @include('global.meta.header')
        <link rel="stylesheet" href="/resources/v/faq/styles/faq.css">
    </head>
    <body>
        @include('global.include.en-US.navigation')
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Frequently Asked Questions?</h1>
                    <dl>
                        <dt>How long has the fishing pool been in existence?</dt>
                        <dd>Our company has been established for <b>8 years</b>, and is one of the original carp fishing excursion outlets in Tenerife.<br>
                                The reservoir has been in existence for 80 years, but was stocked with various species of carp fourteen years ago.</dd>
                    </dl>
                    <dl>
                        <dt>What Carp stock is available in this large pool?</dt>
                        <dd>
                            <ul>
                                <li>Mirror Carp</li>
                                <li>Grass Carp</li>
                                <li>Common Carp</li>
                                <li>Canary Wild Carp</li>
                            </ul>
                        </dd>
                            <div id="billboard-g-slider" class="carousel slide" data-ride="carousel">
                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                    <li data-target="#billboard-g-slider" data-slide-to="0" class="active"></li>
                                    <li data-target="#billboard-g-slider" data-slide-to="1"></li>
                                    <li data-target="#billboard-g-slider" data-slide-to="2"></li>
                                    <li data-target="#billboard-g-slider" data-slide-to="3"></li>
                                    <li data-target="#billboard-g-slider" data-slide-to="4"></li>
                                    <li data-target="#billboard-g-slider" data-slide-to="5"></li>
                                    <li data-target="#billboard-g-slider" data-slide-to="6"></li>
                                </ol>

                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                    <div class="item active">
                                        <img src="/resources/v/home/images/visitor-0.jpg" alt="">
                                    </div>
                                    <div class="item">
                                        <img src="/resources/v/home/images/visitor-1.jpg" alt="">
                                    </div>
                                    <div class="item">
                                        <img src="/resources/v/home/images/visitor-2.jpg" alt="">
                                    </div>
                                    <div class="item">
                                        <img src="/resources/global/images/lake/feature/pl16.jpg" alt="">
                                    </div>
                                    <div class="item">
                                        <img src="/resources/v/home/images/visitor-3.jpg" alt="">
                                    </div>
                                    <div class="item">
                                        <img src="/resources/v/home/images/visitor-4.jpg" alt="">
                                    </div>
                                    <div class="item">
                                        <img src="/resources/v/home/images/visitor-5.jpg" alt="">
                                    </div>
                                </div>

                                <!-- Controls -->
                                <a class="left carousel-control" href="#billboard-g-slider" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                </a>
                                <a class="right carousel-control" href="#billboard-g-slider" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </a>
                            </div>
                    </dl>
                    <dl>
                        <dt>Approximately how many carp are in the pool and range of sizes?</dt>
                        <dd>
                            <ul>
                                <li>There are approximately <b>1500 carp</b></li>
                                <li>The carp range from <b>3/4 Kilo</b> to <b>20 Kilo’s (44 lbs) ‘Atomic Lady’</b> who has a <b>large following</b> of <b>admirers</b> as well as <b>professional anglers</b></li>
                                <li>Recently there has been approximately <b>50 carp</b> that have been added and they range from <b>3.6 Kilo’s to 12 Kilo’s in  size</b></li>
                            </ul>
                        </dd>
                    </dl>
                    <dl>
                        <dt>What Equipment is Provided:</dt>
                        <dd>
                            <ul>
                                <li><b>All tackle and bait supplied by Carpa-Canaria</b></li>
                                <li><b>Is a fishing license required (All taken care of?)</b></li>
                                <li><b>Life jackets are available for young children</b></li>
                                <li><b>Comfortable chairs and umbrellas supplied</b></li>
                                <li><b>Indemnity and Public Liability insurance</b></li>
                                <li><b>Light lunch and refreshments at a reasonable cost</b></li>
                            </ul>
                        </dd>
                    </dl>
                    <dl>
                        <dt>Included in our Day Trips:</dt>
                        <ul>
                            <li><b>Collection from nearby venues</b></li>
                            <li><b>08h30 am (this varies on clients and fish feeding habits!)</b></li>
                            <li><b>Drop off at 13h00 pm</b></li>
                            <li><b>Groups may be arranged at convenient times</b></li>
                        </ul>
                    </dl>
                    <dl>
                        <dt>Some Facts Regarding the Pool</dt>
                        <dd>
                            <p>
                                The pool has 12 pegs on it and the depth is impressive, some areas are 5.48 meters deep.<br>
                                The shallowest is 2.74 meters with shelves of 1.62 meters wide,  which accommodate our seating/fishing guests comfortably. <br>
                                Normally you fish with a sliding float, which is responsive and very popular. <br>
                                A ledger is also used, when the fish are very active, which allows you catch the larger fish when the fish are feeding off the bottom. <br>
                            </p>
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
        @include('global.meta.footer')
    </body>
</html>