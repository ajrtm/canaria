<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US" prefix="og: http://ogp.me/ns#">
    <head>
        <title>Carpa Canaria</title>
        @include('global.meta.header')
        <link rel="stylesheet" href="/resources/v/gallery/styles/gallery.css">
    </head>
    <body>
        @include('global.include.en-US.navigation')
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- SnapWidget -->
                    <script src="http://snapwidget.com/js/snapwidget.js"></script>
                    <iframe src="http://snapwidget.com/in/?u=Y2FycGFfY2FuYXJpYXxpbnwxMjV8NXw1fHxub3w1fG5vbmV8b25TdGFydHx5ZXN8eWVz&ve=011014" title="Instagram Widget" class="snapwidget-widget" allowTransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden; width:100%;"></iframe>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row" style="padding: 30px 0;">
                <div class="col-md-4">
                    <img src="/resources/global/images/rv/v4.jpg" alt="" class="img-responsive">
                </div>
                <div class="col-md-4">
                    <img src="/resources/global/images/rv/v5.jpg" alt="" class="img-responsive">
                </div>
                <div class="col-md-4">
                    <img src="/resources/global/images/rv/v6.jpg" alt="" class="img-responsive">
                </div>
            </div>
            <div class="row" style="padding: 30px 0;">
                <div class="col-md-4">
                    <img src="/resources/global/images/rv/v1.jpg" alt="" class="img-responsive">
                </div>
                <div class="col-md-4">
                    <img src="/resources/global/images/rv/v2.jpg" alt="" class="img-responsive">
                </div>
                <div class="col-md-4">
                    <img src="/resources/global/images/rv/v3.jpg" alt="" class="img-responsive">
                </div>
            </div>
        </div>
        @include('global.meta.footer')
    </body>
</html>