<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US" prefix="og: http://ogp.me/ns#">
    <head>
        <title>Carpa Canaria</title>
        @include('global.meta.header')
        <link rel="stylesheet" href="/resources/v/about/styles/about.css">
    </head>
    <body>
        @include('global.include.en-US.navigation')
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>About Carpa-Canaria Fishing</h1>
                    <p>Tenerife is ideal for freshwater coarse fishing. <b>Carp</b> are the <b>‘BIG Catch’</b> of these leisure expeditions. All in all, hundreds of species of fish live in the surrounding  waters of Tenerife, which inspires the passionate fishermen.</p>
                    <p><b>Carpa-Canaria</b> is located up in the scenic mountains near San Miguel.</p>
                    <p>We offer mini-bus pickups for medium to large groups, which may incorporate a half or full-Day excursion. San Miguel is approximately 15 minute drive out of Los Cristianos.</p>
                    <br>
                    <h2 class="text-center"><b>Common, Mirror, Grass &amp; Wild Canary Carp</b></h2>
                    <p class="text-center"><b>The largest carp is ‘Atomic Lady’ at 44 Lbs, or 20 Kilos ‘Catch Me if You Can’</b></p>
                    <br>
                    <br>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <img src="/resources/global/images/lake/feature/pl6.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-4">
                        <img src="/resources/global/images/lake/feature/pl4.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-4">
                        <img src="/resources/global/images/lake/feature/pl0.jpg" alt="" class="img-responsive">
                    </div>
                </div>
            </div>
        </div>
        @include('global.meta.footer')
    </body>
</html>