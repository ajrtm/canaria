<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US" prefix="og: http://ogp.me/ns#">
    <head>
        <title>Carpa Canaria</title>
        @include('global.meta.header')
        <link rel="stylesheet" href="/resources/v/home/styles/home.css">
        <style>
            #billboard-g-slider.carousel {
                width: 80%;
                float: right;
            }
        </style>
    </head>
    <body>
    @if(Session::has('global'))
    <p class="bg-warning text-center" style="z-index: 9999; color: black; padding-top: 10px; padding-bottom: 10px; position: absolute; top: 0; left: 0; width: 100%;"><b>Information: </b>{{ Session::get('global') }}</p>
    @endif
        @include('global.include.en-US.navigation')
        <div class="container-fluid" id="billboard">
            <div class="row">
                <div class="billboard-container">
                    <div class="billboard-image">
                        <div id="rotate-header" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators hidden">
                                <li data-target="#rotate-header" data-slide-to="0" class="active"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img src="/resources/global/images/rv/v1.jpg" alt="">
                                </div>
                            </div>
                            <a class="left carousel-control hidden" href="#rotate-header" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control hidden" href="#rotate-header" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div>
                    </div>
                    <div class="container billboard-content">
                        <div class="col-md-6 content">
                            <h1>All year-round fishing in Tenerife, Canary Islands</h1>
                            <p>Fishing for the whole family, including fishing tackle, from novice to experienced fisherman. Air conditioned mini-bus transfer available from your hotel or venue</p>
                        </div>
                        <div class="col-md-6">
                            <div id="billboard-g-slider" class="carousel slide" data-ride="carousel">
                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                    <li data-target="#billboard-g-slider" data-slide-to="0" class="active"></li>
                                    <li data-target="#billboard-g-slider" data-slide-to="1"></li>
                                    <li data-target="#billboard-g-slider" data-slide-to="2"></li>
                                    <li data-target="#billboard-g-slider" data-slide-to="3"></li>
                                    <li data-target="#billboard-g-slider" data-slide-to="4"></li>
                                    <li data-target="#billboard-g-slider" data-slide-to="5"></li>
                                    <li data-target="#billboard-g-slider" data-slide-to="6"></li>
                                </ol>

                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                    <div class="item active">
                                        <img src="/resources/v/home/images/visitor-0.jpg" alt="">
                                    </div>
                                    <div class="item">
                                        <img src="/resources/v/home/images/visitor-1.jpg" alt="">
                                    </div>
                                    <div class="item">
                                        <img src="/resources/v/home/images/visitor-2.jpg" alt="">
                                    </div>
                                    <div class="item">
                                        <img src="/resources/global/images/lake/feature/pl16.jpg" alt="">
                                    </div>
                                    <div class="item">
                                        <img src="/resources/v/home/images/visitor-3.jpg" alt="">
                                    </div>
                                    <div class="item">
                                        <img src="/resources/v/home/images/visitor-4.jpg" alt="">
                                    </div>
                                    <div class="item">
                                        <img src="/resources/v/home/images/visitor-5.jpg" alt="">
                                    </div>
                                </div>

                                <!-- Controls -->
                                <a class="left carousel-control" href="#billboard-g-slider" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                </a>
                                <a class="right carousel-control" href="#billboard-g-slider" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container pull-top">
            <div class="row">
                <div class="col-md-12">
                    <div id="billboard-mobile" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#billboard-mobile" data-slide-to="0" class="active"></li>
                            <li data-target="#billboard-mobile" data-slide-to="1"></li>
                            <li data-target="#billboard-mobile" data-slide-to="2"></li>
                            <li data-target="#billboard-mobile" data-slide-to="3"></li>
                            <li data-target="#billboard-mobile" data-slide-to="4"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img src="/resources/global/images/lake/feature/pl7.jpg" alt="">
                                </div>
                                <div class="item">
                                    <img src="/resources/global/images/lake/feature/pl8.jpg" alt="">
                                </div>
                                <div class="item">
                                    <img src="/resources/global/images/lake/feature/pl10.jpg" alt="">
                                </div>
                                <div class="item">
                                    <img src="/resources/global/images/lake/feature/pl11.jpg" alt="">
                                </div>
                                <div class="item">
                                    <img src="/resources/global/images/lake/feature/pl15.jpg" alt="">
                                </div>
                            </div>
                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#billboard-mobile" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#billboard-mobile" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <h1>Welcome to Carpa Canaria!</h1>
                    <p>Tenerife offers exceptional weather all year-round, with glorious sunshine on most days, kissed by gentle coastal breezes. With stunning sandy beaches on the coast, and a spectacular inland landscape of mountains and lakes, this island offers the perfect setting for a memorable fishing holiday.</p>
                    <p>We offer day-fishing trips for those of you who are already booked on holiday; and we will pick you up from your hotel or venue in the morning and drop you back in the evening. We offer a large variety of excursions and tours, all tailored to fit your needs, creating a holiday experience of a lifetime! Book now with us, and then sit back and relax...because this is YOUR holiday, and we work very hard to make it the very best!</p>
                    <p>One needs to experience this to appreciate it. For a true taste of adventure contact Carpa Canaria!</p>
                </div>
                <div class="col-md-8 mobile-gallery">
                    <div id="home-gallery" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#home-gallery" data-slide-to="0" class="active"></li>
                            <li data-target="#home-gallery" data-slide-to="1"></li>
                            <li data-target="#home-gallery" data-slide-to="2"></li>
                            <li data-target="#home-gallery" data-slide-to="3"></li>
                            <li data-target="#home-gallery" data-slide-to="4"></li>
                            <li data-target="#home-gallery" data-slide-to="5"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <img src="/resources/v/home/images/visitor-0.jpg" alt="">
                            </div>
                            <div class="item">
                                <img src="/resources/v/home/images/visitor-1.jpg" alt="">
                            </div>
                            <div class="item">
                                <img src="/resources/v/home/images/visitor-2.jpg" alt="">
                            </div>
                            <div class="item">
                                <img src="/resources/v/home/images/visitor-3.jpg" alt="">
                            </div>
                            <div class="item">
                                <img src="/resources/v/home/images/visitor-4.jpg" alt="">
                            </div>
                            <div class="item">
                                <img src="/resources/v/home/images/visitor-5.jpg" alt="">
                            </div>
                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#home-gallery" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#home-gallery" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row" style="padding: 30px 0;">
                <div class="col-md-12">
                    <img src="/resources/global/images/rv/v0.jpg" alt="" class="img-responsive">
                </div>
            </div>
            <div class="row" style="padding: 30px 0;">
                <div class="col-md-4">
                    <img src="/resources/global/images/rv/v7.jpg" alt="" class="img-responsive">
                </div>
                <div class="col-md-4">
                    <img src="/resources/global/images/rv/v2.jpg" alt="" class="img-responsive">
                </div>
                <div class="col-md-4">
                    <img src="/resources/global/images/rv/v3.jpg" alt="" class="img-responsive">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>You can find our flyers in some of the hotel receptions!</h1>
                    <p>excursion outlets or can simply email to pre-book your excursion in advance to secure booking. Because the "Carp Pool" is crammed full of carp and it's really easy to make a catch, it is suitable for beginners and children, as well as experienced anglers.</p>
                    <p>Carpa Canaria is equipped to accommodate both day fishing groups and night fishing on request.</p>
                    <ul>
                        <li>Originally a large holding tank for the grape plantations</li>
                        <li>Approximately 1600 carp, ranging from 2lbs to the record size 42lbs + 'Atomic Lady' her reputation proceeds her</li>
                    </ul>
                    <b>Restrictions</b>
                    <ul>
                        <li>10lbs line minimum, barbless hooks only and rods supplied</li>
                        <li>permits are generally included, unless otherwise stipulated</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row" style="padding: 30px 0;">
            <div class="col-md-4">
                <img src="/resources/global/images/rv/v4.jpg" alt="" class="img-responsive">
            </div>
            <div class="col-md-4">
                <img src="/resources/global/images/rv/v5.jpg" alt="" class="img-responsive">
            </div>
            <div class="col-md-4">
                <img src="/resources/global/images/rv/v6.jpg" alt="" class="img-responsive">
            </div>
        </div>
        @include('global.meta.footer')
    </body>
</html>