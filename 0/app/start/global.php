<?php

/*
|--------------------------------------------------------------------------
| Register The Laravel Class Loader
|--------------------------------------------------------------------------
|
| In addition to using Composer, you may use the Laravel class loader to
| load your controllers and models. This is useful for keeping all of
| your classes in the "global" namespace without Composer updating.
|
*/

ClassLoader::addDirectories(array(

	app_path().'/commands',
	app_path().'/controllers',
	app_path().'/models',
	app_path().'/database/seeds',

));

/*
|--------------------------------------------------------------------------
| Application Error Logger
|--------------------------------------------------------------------------
|
| Here we will configure the error logger setup for the application which
| is built on top of the wonderful Monolog library. By default we will
| build a basic log file setup which creates a single file for logs.
|
*/

Log::useFiles(storage_path().'/logs/laravel.log');

/*
|--------------------------------------------------------------------------
| Application Error Handler
|--------------------------------------------------------------------------
|
| Here you may handle any errors that occur in your application, including
| logging them or displaying custom views for specific errors. You may
| even register several error handlers to handle different types of
| exceptions. If nothing is returned, the default error view is
| shown, which includes a detailed stack trace during debug.
|
*/

App::missing(function($exception)
{
    return Response::view('global.missing', array(), 404);
});

App::error(function(Exception $exception, $code)
{
	Log::error($exception);
});

App::fatal(function($exception)
{
    // Send email
    Mail::send('global.action', array(), function($message)
    {
        $message->to("info@carpacanaria.com", "Carpa-Canaria Site")->cc("hello@developka.com")->subject('Action Required: Server Problem / Unrecoverable Error');
        $message->from("hello@developka.com", "Service Agent (Carpa-Canaria Fishing)");
    });

    // Send the customer to the status page
    return Redirect::to('http://status.carpacanaria.com/');
});

/*
|--------------------------------------------------------------------------
| Maintenance Mode Handler
|--------------------------------------------------------------------------
|
| The "down" Artisan command gives you the ability to put an application
| into maintenance mode. Here, you will define what is displayed back
| to the user if maintenance mode is in effect for the application.
|
*/

App::down(function()
{
	return Response::make("<h1>Resource Unavailable</h1><br>The resource requested is not available, contact your server administrator if this is unexpected behaviour.<hr>UUD ID <i>100987719719</i> can be used when contacting support. <a href='mailto:theo@developka.com'>theo@developka.com</a>", 503);
});

/*
|--------------------------------------------------------------------------
| Require The Filters File
|--------------------------------------------------------------------------
|
| Next we will load the filters file for the application. This gives us
| a nice separate location to store our route and application filter
| definitions instead of putting them all in the main routes file.
|
*/

require app_path().'/filters.php';
